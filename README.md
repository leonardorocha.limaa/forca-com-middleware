# Forca com Middleware
Alunos: Leonardo Kawasaki e Leonardo Lima.

Projeto da matéria de Tópicos Avançados em Computação.

## Sobre

Implementação de um jogo de forca, baseado no roda-a-roda Jequiti, utilizando um middleware e os conceitos de sistemas distribuídos.

## Tecnologias Utilizadas

Java 13, utilizando Sockets e ServerSockets.

#### Rodando a versão 1:

* Recomenda-se o uso da versão 13 do Java
* Rode a classe Main localizada em src/forca/unicoprocesso/Main.java
* Para jogar use o terminal CLI de sua IDE

#### Rodando a versão 2:

* Recomenda-se o uso da versão 13 do Java
* Rode, nesse ordem as classes Servidor, Cliente1, Cliente2 e Cliente3 localizadas em src/forca/emredes/
* As classes deverão estar rodando simultaneamente
* Para jogar use o terminal CLI de sua IDE
