package forca.unicoprocesso.partida;

import java.util.*;

import forca.unicoprocesso.jogador.Jogador;
import leitor.Leitor;

public class Partida {

    private static final int PONTUACAO_MAXIMA = 3000;
    private static int pontuacaoRodada;
    private static final Jogador jogador1 = new Jogador(1, 0);
    private static final Jogador jogador2 = new Jogador(2, 0);
    private static final Jogador jogador3 = new Jogador(3, 0);
    private static List<String> palavras = new ArrayList<>();
    private static String letrasUsadas = "";
    private static List<String> letrasCertas = new ArrayList<>();
    private static final Leitor leitor = new Leitor();

    public static void iniciar() {
        System.out.println("*****Bem vindo ao jogo da Forca com Middleware!*****");
        System.out.println();
        System.out.println("Dica: Frutas");
        System.out.println();
        leitor.lerArquivo("src/arquivos/banco-de-palavras.txt");
        palavras = leitor.verificaPalavras();
        leitor.printPalavrasSombreadas(palavras);

        // Apagar depois:
        System.out.println(palavras);

        while (jogador1.getPontuacao() < PONTUACAO_MAXIMA &&
                jogador2.getPontuacao() < PONTUACAO_MAXIMA &&
                jogador3.getPontuacao() < PONTUACAO_MAXIMA) {
            jogada(jogador1);
            jogada(jogador2);
            jogada(jogador3);
        }
    }

    public static void jogada(Jogador jogador) {
        Scanner scanner = new Scanner(System.in);

        if (jogador.getPontuacao() >= 2500) {
            chutarPalavras(jogador);
        }

        gerarPontuacaoRodada();
        System.out.println();
        System.out.println("Letras já usadas: " + letrasUsadas);
        System.out.println("Valendo " + pontuacaoRodada + " pontos, Jogador "
                + jogador.getId() +" , digite uma letra: ");
        String letra = scanner.nextLine();

        if (letrasUsadas.contains(letra)) {
            System.out.println("Passou a vez!!!");
            System.out.println();
            if (jogador.getId() == 1) {
                jogada(jogador2);
            } else if (jogador.getId() == 2) {
                jogada(jogador3);
            } else {
                jogada(jogador1);
            }
        } else {
            letrasUsadas += letra + ", ";
            boolean contem = contemLetra(palavras, letra);
            if (contem) {
                letrasCertas.add(letra);
                System.out.println(letrasCertas);
                leitor.mostrarLetras(palavras, letrasCertas);
                // letrasCertas = letrasCertas.subList(0, letrasCertas.size());
                letrasCertas = new ArrayList<>();

                jogador.setPontuacao(jogador.getPontuacao() + pontuacaoRodada);
                System.out.println("Pontuação Jogador " + jogador.getId() + ": " + jogador.getPontuacao());
                System.out.println();
                if (jogador.getPontuacao() > PONTUACAO_MAXIMA) {
                    fimDeJogo(jogador);
                }
                jogada(jogador);
            } else {
                System.out.println();
            }
        }
    }

    private static boolean contemLetra(List<String> palavras, String letra) {
        boolean contem = false;
        int contador = 0;
        for (String s : palavras) {
            String[] palavra = s.split("");
            for (String letraAtual : palavra) {
                if (letraAtual.equals(letra)) {
                    contador++;
                    contem = true;
                }
            }
        }
        System.out.println("Existem " + contador + " letras " + letra);
        return contem;
    }

    public static void chutarPalavras(Jogador jogador) {
        System.out.println("Sua chance de vencer " + jogador.getId() + "!");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite a primeira palavra: ");
        String chute1 = scanner.nextLine();
        System.out.println("Digite a segunda palavra: ");
        String chute2 = scanner.nextLine();
        System.out.println("Digite a terceira palavra: ");
        String chute3 = scanner.nextLine();

        if (palavras.contains(chute1) && palavras.contains(chute2) && palavras.contains(chute3)) {
            fimDeJogo(jogador);
        } else {
            if (jogador.getId() == 1) {
                jogada(jogador2);
            } else if (jogador.getId() == 2) {
                jogada(jogador3);
            } else {
                jogada(jogador1);
            }
        }
    }

    public static void fimDeJogo(Jogador jogador) {
        System.out.println(".");
        System.out.println("..");
        System.out.println("...");
        System.out.println("....");
        System.out.println(".....");
        System.out.println(".......");
        System.out.println("........");
        System.out.println("Fim de Jogo!");
        System.out.println("Vitória do jogador " + jogador.getId() + "!!!!!!!!!!!!!");
        System.exit(0);
    }

    private static void gerarPontuacaoRodada() {
        Random random = new Random();
        pontuacaoRodada = random.nextInt(1000);
    }
}
