package forca.unicoprocesso.jogador;

public class Jogador {
    int id;
    int pontuacao;

    public Jogador(int id, int pontuacao) {
        this.id = id;
        this.pontuacao = pontuacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
}
