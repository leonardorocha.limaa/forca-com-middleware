package forca.emredes.servidor;

import forca.emredes.jogador.Jogador;
import leitor.Leitor;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Servidor {

    private static final int PONTUACAO_MAXIMA = 3000;
    private static int pontuacaoRodada;
    private static final Jogador jogador1 = new Jogador(1, 0);
    private static final Jogador jogador2 = new Jogador(2, 0);
    private static final Jogador jogador3 = new Jogador(3, 0);
    private static List<String> palavras = new ArrayList<>();
    private static String letrasUsadas = "";
    private static List<String> letrasCertas = new ArrayList<>();
    private static final Leitor leitor = new Leitor();

    private static ServerSocket serverSocket = null;
    static {
        try {
            serverSocket = new ServerSocket(7777);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Socket socket;
    static {
        try {
            socket = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Socket socket2;
    static {
        try {
            socket2 = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Socket socket3;
    static {
        try {
            socket3 = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static InputStream input;
    static {
        try {
            input = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static InputStream input2;
    static {
        try {
            input2 = socket2.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static InputStream input3;
    static {
        try {
            input3 = socket3.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static OutputStream output;
    static {
        try {
            output = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static OutputStream output2;
    static {
        try {
            output2 = socket2.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static OutputStream output3;
    static {
        try {
            output3 = socket3.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final BufferedReader in1 = new BufferedReader(new InputStreamReader(input));
    private static final BufferedReader in2 = new BufferedReader(new InputStreamReader(input2));
    private static final BufferedReader in3 = new BufferedReader(new InputStreamReader(input3));
    private static final PrintStream out = new PrintStream(output);
    private static final PrintStream out2 = new PrintStream(output2);
    private static final PrintStream out3 = new PrintStream(output3);

    public Servidor() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        System.out.println("*****Bem vindo ao jogo da Forca com Middleware!*****");
        System.out.println();
        System.out.println("Dica: Frutas");
        leitor.lerArquivo("src/arquivos/banco-de-palavras.txt");
        palavras = leitor.verificaPalavras();
        leitor.printPalavrasSombreadas(palavras);

        // Apagar depois:
        System.out.println(palavras);

        while (jogador1.getPontuacao() < PONTUACAO_MAXIMA &&
                jogador2.getPontuacao() < PONTUACAO_MAXIMA &&
                jogador3.getPontuacao() < PONTUACAO_MAXIMA) {
            jogada(jogador1);
            jogada(jogador2);
            jogada(jogador3);
        }
    }

    public static void jogada(Jogador jogador) throws IOException {
        if (jogador.getPontuacao() >= 2500) {
            chutarPalavras(jogador);
        }

        gerarPontuacaoRodada();
        System.out.println();
        System.out.println("Letras já usadas: " + letrasUsadas);
        System.out.println("Valendo " + pontuacaoRodada + " pontos, Jogador "
                + jogador.getId() +" , digite uma letra. ");

        String letra;
        if (jogador.getId() == 1) {
            letra = in1.readLine();
            out.println(letra);
        } else if (jogador.getId() == 2) {
            letra = in2.readLine();
            out2.println(letra);
        } else {
            letra = in3.readLine();
            out3.println(letra);
        }

        if (letrasUsadas.contains(letra)) {
            System.out.println("Passou a vez!!!");
            System.out.println();
            if (jogador.getId() == 1) {
                jogada(jogador2);
            } else if (jogador.getId() == 2) {
                jogada(jogador3);
            } else {
                jogada(jogador1);
            }
        } else {
            letrasUsadas += letra + ", ";
            boolean contem = contemLetra(palavras, letra);
            if (contem) {
                letrasCertas.add(letra);
                System.out.println(letrasCertas);
                leitor.mostrarLetras(palavras, letrasCertas);
                letrasCertas = new ArrayList<>();

                jogador.setPontuacao(jogador.getPontuacao() + pontuacaoRodada);
                System.out.println("Pontuação Jogador " + jogador.getId() + ": " + jogador.getPontuacao());
                System.out.println();
                if (jogador.getPontuacao() > PONTUACAO_MAXIMA) {
                    fimDeJogo(jogador);
                }
                jogada(jogador);
            } else {
                System.out.println();
            }
        }
    }

    private static boolean contemLetra(List<String> palavras, String letra) {
        boolean contem = false;
        int contador = 0;
        for (String s : palavras) {
            String[] palavra = s.split("");
            for (String letraAtual : palavra) {
                if (letraAtual.equals(letra)) {
                    contador++;
                    contem = true;
                }
            }
        }
        System.out.println("Existem " + contador + " letras " + letra);
        return contem;
    }

    public static void chutarPalavras(Jogador jogador) throws IOException {
        System.out.println("Sua chance de vencer " + jogador.getId() + "!");

        String chute1 = "";
        String chute2 = "";
        String chute3 = "";
        if (jogador.getId() == 1) {
            System.out.println("Digite a primeira palavra: ");
            chute1 = in1.readLine();
            out.println(chute1);
            System.out.println("Digite a segunda palavra: ");
            chute2 = in1.readLine();
            out.println(chute2);
            System.out.println("Digite a terceira palavra: ");
            chute3 = in1.readLine();
            out.println(chute3);
        } else if (jogador.getId() == 2) {
            System.out.println("Digite a primeira palavra: ");
            chute1 = in2.readLine();
            out2.println(chute1);
            System.out.println("Digite a segunda palavra: ");
            chute2 = in2.readLine();
            out2.println(chute2);
            System.out.println("Digite a terceira palavra: ");
            chute3 = in2.readLine();
            out2.println(chute3);
        } else {
            System.out.println("Digite a primeira palavra: ");
            chute1 = in3.readLine();
            out3.println(chute1);
            System.out.println("Digite a segunda palavra: ");
            chute2 = in3.readLine();
            out3.println(chute2);
            System.out.println("Digite a terceira palavra: ");
            chute3 = in3.readLine();
            out3.println(chute3);
        }

        if (palavras.contains(chute1) && palavras.contains(chute2) && palavras.contains(chute3)) {
            fimDeJogo(jogador);
        } else {
            if (jogador.getId() == 1) {
                jogada(jogador2);
            } else if (jogador.getId() == 2) {
                jogada(jogador3);
            } else {
                jogada(jogador1);
            }
        }
    }

    public static void fimDeJogo(Jogador jogador) {
        System.out.println(".");
        System.out.println("..");
        System.out.println("...");
        System.out.println("....");
        System.out.println(".....");
        System.out.println(".......");
        System.out.println("........");
        System.out.println("Fim de Jogo!");
        System.out.println("Vitória do jogador " + jogador.getId() + "!!!!!!!!!!!!!");
        System.exit(0);
    }

    private static void gerarPontuacaoRodada() {
        Random random = new Random();
        pontuacaoRodada = random.nextInt(1000);
    }
}
