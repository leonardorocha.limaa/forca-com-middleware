package forca.emredes.cliente;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Cliente3 {

    private int id;
    private int pontuacao;

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 7777);
        System.out.println("Bem vindo jogador 3!");

        InputStream input = socket.getInputStream();
        OutputStream output = socket.getOutputStream();

        BufferedReader in = new BufferedReader(new InputStreamReader(input));
        PrintStream out = new PrintStream(output);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Digite uma letra:");
            String mensagem = scanner.nextLine();

            out.println(mensagem);

            if("FIM".equals(mensagem)) {
                break;
            }
            mensagem = in.readLine();

            System.out.println("Mensagem enviada: " + mensagem);
        }
        System.out.println("Encerrando conexão");
        in.close();
        out.close();
        socket.close();
    }

    public Cliente3(int id, int pontuacao) {
        this.id = id;
        this.pontuacao = pontuacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
}
